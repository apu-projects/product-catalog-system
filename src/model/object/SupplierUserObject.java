package model.object;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SupplierUserObject {
    protected int supplierID;
    protected String supplierName;
    public static ObservableList<SupplierUserObject> supplierList = FXCollections.observableArrayList();
    public static ObservableList<SupplierUserObject> supplierSelectList = FXCollections.observableArrayList();

    public SupplierUserObject(String supplierName, int supplierID) {
        this.supplierID = supplierID;
        this.supplierName = supplierName;
    }

    public int getSupplierID() { return supplierID; }
    public String getSupplierName() { return supplierName; }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Override
    public String toString() {
        return String.format("%s|%s",
                supplierName, supplierID);
    }
}
