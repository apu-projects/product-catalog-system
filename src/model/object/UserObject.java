package model.object;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UserObject {
    protected int userID;
    protected String username;
    protected String password;
    protected String role;
    protected String address;
    protected String contactNumber;
    protected String emailAddress;
    public static ObservableList<UserObject> userList = FXCollections.observableArrayList();
    public static ObservableList<UserObject> userSelectList = FXCollections.observableArrayList();

    public UserObject() {

    }

    public UserObject(int userID, String username, String password, String address,
                      String contactNumber, String emailAddress, String role) {
        this.userID = userID;
        this.username = username;
        this.password = password;
        this.role = role;
        this.address = address;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
    }

    public int getUserID() { return userID; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getRole() { return role; }
    public String getAddress() { return address; }
    public String getContactNumber() { return contactNumber; }
    public String getEmailAddress() { return emailAddress; }

    public void setUserID(int userID) { this.userID = userID; }
    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }
    public void setRole(String role) { this.role = role; }
    public void setAddress(String address) { this.address = address; }
    public void setContactNumber(String contactNumber) { this.contactNumber = contactNumber; }
    public void setEmailAddress(String emailAddress) { this.emailAddress = emailAddress; }

    //Writing format
    @Override
    public String toString() {
        return String.format("%s|%s|%s|%s|%s|%s|%s",
                userID, username, password, address,
                contactNumber, emailAddress, role);
    }
}