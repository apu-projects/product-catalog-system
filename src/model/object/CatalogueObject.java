package model.object;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.time.LocalDate;

public class CatalogueObject {
    protected int catalogueId;
    protected String catalogueName;
    protected LocalDate catalogueDateStart;
    protected LocalDate catalogueDateEnd;
    protected String catalogueDescription;
    public static ObservableList<CatalogueObject> catalogueList = FXCollections.observableArrayList();
    public static ObservableList<CatalogueObject> catalogueSelectList = FXCollections.observableArrayList();

    public CatalogueObject(String catalogueName, int catalogueId, LocalDate catalogueDateStart,
                           LocalDate catalogueDateEnd, String catalogueDescription) {
        this.catalogueId = catalogueId;
        this.catalogueName = catalogueName;
        this.catalogueDateStart = catalogueDateStart;
        this.catalogueDateEnd = catalogueDateEnd;
        this.catalogueDescription = catalogueDescription;
    }

    public int getCatalogueId() { return catalogueId; }
    public String getCatalogueName() { return catalogueName; }
    public LocalDate getCatalogueDateStart() { return catalogueDateStart; }
    public LocalDate getCatalogueDateEnd() { return catalogueDateEnd; }
    public String getCatalogueDescription() { return catalogueDescription; }

    public void setCatalogueId(int catalogueId) { this.catalogueId = catalogueId; }
    public void setCatalogueName(String catalogueName) { this.catalogueName = catalogueName; }
    public void setCatalogueDateStart(LocalDate catalogueDateStart) { this.catalogueDateStart = catalogueDateStart; }
    public void setCatalogueDateEnd(LocalDate catalogueDateEnd) { this.catalogueDateEnd = catalogueDateEnd; }
    public void setCatalogueDescription(String catalogueDescription) { this.catalogueDescription = catalogueDescription; }

    @Override
    public String toString() {
        return String.format("%s|%s|%s|%s|%s",
                catalogueName, catalogueId, catalogueDateStart, catalogueDateEnd,
                catalogueDescription);
    }
}
