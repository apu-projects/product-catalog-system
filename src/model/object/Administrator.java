package model.object;

public class Administrator extends UserObject {

    public Administrator(int userId, String username, String password, String address,
                         String contactNumber, String emailAddress, String role) {
        this.userID = userId;
        this.username = username;
        this.password = password;
        this.role = role;
        this.address = address;
        this.contactNumber = contactNumber;
        this.emailAddress = emailAddress;
    }
}
