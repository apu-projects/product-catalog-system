package model.object;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProductObject {
    protected int productID;
    protected String productName;
    protected String productCategory;
    protected int productQuantity;
    protected double productRetailPrice;
    protected String supplierName;
    protected String productCatalogue;

    public static ObservableList<ProductObject> productList = FXCollections.observableArrayList();
    public static ObservableList<ProductObject> productSelectList = FXCollections.observableArrayList();

    public ProductObject() {

    }

    public ProductObject(int productID, String productName, String productCategory, int productQuantity,
                         double productRetailPrice, String supplierName, String productCatalogue) {
        this.productID = productID;
        this.productName = productName;
        this.productCategory = productCategory;
        this.productQuantity = productQuantity;
        this.productRetailPrice = productRetailPrice;
        this.supplierName = supplierName;
        this.productCatalogue = productCatalogue;
    }

    public int getProductID() { return productID; }
    public String getProductName() { return productName; }
    public String getProductCategory() { return productCategory; }
    public int getProductQuantity() { return productQuantity; }
    public double getProductRetailPrice() { return productRetailPrice; }
    public String getSupplierName() { return supplierName; }
    public String getProductCatalogue() { return productCatalogue; }

    public void setProductID(int productID) { this.productID = productID; }
    public void setProductName(String productName) { this.productName = productName; }
    public void setProductCategory(String productCategory) { this.productCategory = productCategory; }
    public void setProductQuantity(int productQuantity) { this.productQuantity = productQuantity; }
    public void setProductRetailPrice(double productRetailPrice) { this.productRetailPrice = productRetailPrice; }
    public void setSupplierName(String supplierName) { this.supplierName = supplierName; }
    public void setProductCatalogue(String productCatalogue) { this.productCatalogue = productCatalogue; }

    @Override
    public String toString() {
        return String.format("%s|%s|%s|%s|%s|%s|%s|",
                productID, productName, productCategory, productQuantity,
                productRetailPrice, supplierName, productCatalogue);
    }
}
