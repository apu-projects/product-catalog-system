package model.object;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CategoryObject {
    protected int categoryID;
    protected String categoryName;
    protected String categoryDescription;
    public static ObservableList<CategoryObject> categoryList = FXCollections.observableArrayList();
    public static ObservableList<CategoryObject> categorySelectList = FXCollections.observableArrayList();

    public CategoryObject(String categoryName, int categoryID, String categoryDescription) {
        this.categoryID = categoryID;
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
    }

    public int getCategoryID() { return categoryID; }
    public String getCategoryName() { return categoryName; }
    public String getCategoryDescription() { return categoryDescription; }

    public void setCategoryID(int categoryID) { this.categoryID = categoryID; }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    @Override
    public String toString() {
        return String.format("%s|%s|%s",
                categoryName, categoryID, categoryDescription);
    }
}