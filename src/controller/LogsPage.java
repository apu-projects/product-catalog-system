package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextArea;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class LogsPage implements Initializable {
    @FXML TextArea logTextArea;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Scanner s = new Scanner(new File(String.valueOf(FileManagerController.getLog()))).useDelimiter("\\|");
            while (s.hasNext()) {
                if (s.hasNextInt()) { // check if next token is an int
                    logTextArea.appendText(s.nextInt() + " "); // display the found integer
                } else {
                    logTextArea.appendText(s.next() + " "); // else read the next token
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
        }
    }

    public void LogsBackButton(ActionEvent actionEvent) {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }
}
