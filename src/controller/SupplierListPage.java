package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.object.CategoryObject;
import model.object.ProductObject;
import model.object.SupplierUserObject;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class SupplierListPage implements Initializable {
    @FXML TableView supplierTableView;
    @FXML TableColumn supplierIDTable;
    @FXML TableColumn supplierNameTable;

    @FXML TextField supplierIDTextField;
    @FXML TextField supplierNameTextField;

    @FXML ComboBox SupplierStatusComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        supplierIDTable.setCellValueFactory( new PropertyValueFactory<>("supplierID"));
        supplierNameTable.setCellValueFactory( new PropertyValueFactory<>("supplierName"));
        supplierTableView.setItems(SupplierUserObject.supplierList);

        SupplierStatusComboBox.getItems().add("true");
        SupplierStatusComboBox.getItems().add("false");
    }

    public void supplierSelectClick(MouseEvent mouseEvent) {
        SupplierUserObject.supplierSelectList = supplierTableView.getSelectionModel().getSelectedItems();
        supplierIDTextField.setText(String.valueOf(SupplierUserObject.supplierSelectList.get(0).getSupplierID()));
        supplierNameTextField.setText(SupplierUserObject.supplierSelectList.get(0).getSupplierName());
    }

    public void supplierUpdateButton(ActionEvent actionEvent) throws IOException {
        SupplierUserObject supplierUserObject = (SupplierUserObject) supplierTableView.getSelectionModel().getSelectedItem();
        supplierUserObject.setSupplierID(Integer.parseInt(supplierIDTextField.getText()));
        supplierUserObject.setSupplierName(supplierNameTextField.getText());

        FileManagerController.ProductPageWriter("Updated product");

        String searchName = String.valueOf(supplierUserObject.getSupplierName());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getSupplier())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.add(supplierUserObject.getSupplierName() + "|" +
                                supplierUserObject.getSupplierID());
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getSupplier())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void supplierDeleteButton(ActionEvent actionEvent) throws IOException {
        supplierTableView.getItems().removeAll(supplierTableView.getSelectionModel().getSelectedItems());
        FileManagerController.ProductPageWriter("Deleted supplier");

        String searchName = String.valueOf(supplierNameTextField.getText());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getSupplier())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.toString().trim();
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getSupplier())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void supplierAddButton(ActionEvent actionEvent) throws IOException {
        SupplierUserObject.supplierList.add(new SupplierUserObject(supplierNameTextField.getText()
                ,Integer.parseInt(supplierIDTextField.getText())));
        FileManagerController.ProductPageWriter("Added supplier");

        PrintWriter pw = new PrintWriter(new FileWriter(FileManagerController.getSupplier(), true));
        pw.println(SupplierUserObject.supplierList.get(SupplierUserObject.supplierList.size()-1));
        pw.close();
    }

    public void supplierBackButton(ActionEvent actionEvent) throws IOException {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }
}
