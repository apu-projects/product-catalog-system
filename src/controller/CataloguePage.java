package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.object.CatalogueObject;
import model.object.CategoryObject;
import model.object.ProductObject;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class CataloguePage implements Initializable {

    @FXML TableView catalogueITableView;
    @FXML TableColumn CatalogueIDTable;
    @FXML TableColumn catalogueNameTable;
    @FXML TableColumn catalogueSTartDateTable;
    @FXML TableColumn CatalogueEndDateTable;
    @FXML TableColumn CatalogueDescriptionTable;

    @FXML TextField catalogueIDTextField;
    @FXML TextField catalogueNameTextField;
    @FXML TextField catalogueDescriptionTextField;

    @FXML DatePicker catalogueStartDate;
    @FXML DatePicker catalogueEndDate;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CatalogueIDTable.setCellValueFactory( new PropertyValueFactory<>("catalogueId"));
        catalogueNameTable.setCellValueFactory( new PropertyValueFactory<>("catalogueName"));
        catalogueSTartDateTable.setCellValueFactory( new PropertyValueFactory<>("catalogueDateStart"));
        CatalogueEndDateTable.setCellValueFactory( new PropertyValueFactory<>("catalogueDateEnd"));
        CatalogueDescriptionTable.setCellValueFactory( new PropertyValueFactory<>("catalogueDescription"));
        catalogueITableView.setItems(CatalogueObject.catalogueList);
    }

    public void catalogueSelectClick(MouseEvent mouseEvent) {
        CatalogueObject.catalogueSelectList = catalogueITableView.getSelectionModel().getSelectedItems();
        catalogueIDTextField.setText(String.valueOf(CatalogueObject.catalogueSelectList.get(0).getCatalogueId()));
        catalogueNameTextField.setText(CatalogueObject.catalogueSelectList.get(0).getCatalogueName());
        catalogueStartDate.setValue(LocalDate.parse(String.valueOf(CatalogueObject.catalogueSelectList.get(0).getCatalogueDateStart())));
        catalogueEndDate.setValue(LocalDate.parse(String.valueOf(CatalogueObject.catalogueSelectList.get(0).getCatalogueDateEnd())));
        catalogueDescriptionTextField.setText(CatalogueObject.catalogueSelectList.get(0).getCatalogueDescription());
    }

    public void catalogueUpdateButton(ActionEvent actionEvent) throws IOException {
        CatalogueObject catalogueObject = (CatalogueObject) catalogueITableView.getSelectionModel().getSelectedItem();
        catalogueObject.setCatalogueId(Integer.parseInt(catalogueIDTextField.getText()));
        catalogueObject.setCatalogueName(catalogueNameTextField.getText());
        catalogueObject.setCatalogueDateStart(catalogueStartDate.getValue());
        catalogueObject.setCatalogueDateEnd(catalogueEndDate.getValue());
        catalogueObject.setCatalogueDescription(catalogueDescriptionTextField.getText());

        FileManagerController.ProductPageWriter("Updated product");

        String searchName = catalogueObject.getCatalogueName();
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getCatalogue())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.add(catalogueObject.getCatalogueName() + "|" +
                                catalogueObject.getCatalogueId() + "|" +
                                catalogueObject.getCatalogueDateStart() + "|" +
                                catalogueObject.getCatalogueDateEnd() + "|" +
                                catalogueObject.getCatalogueDescription());
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getCatalogue())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void catalogueDeleteButton(ActionEvent actionEvent) throws IOException {
        catalogueITableView.getItems().removeAll(catalogueITableView.getSelectionModel().getSelectedItems());
        FileManagerController.ProductPageWriter("Deleted catalogue");

        String searchName = catalogueNameTextField.getText();
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getCatalogue())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.toString().trim();
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getCatalogue())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void catalogueAddButton(ActionEvent actionEvent) throws IOException {
        CatalogueObject.catalogueList.add(new CatalogueObject(catalogueNameTextField.getText(),Integer.parseInt(catalogueIDTextField.getText()),
                catalogueStartDate.getValue(),catalogueEndDate.getValue(),catalogueDescriptionTextField.getText()));
        FileManagerController.ProductPageWriter("Added catalogue");

        PrintWriter pw = new PrintWriter(new FileWriter(FileManagerController.getCatalogue(), true));
        pw.println(CatalogueObject.catalogueList.get(CatalogueObject.catalogueList.size()-1));
        pw.close();
    }

    public void catalogueBackButton(ActionEvent actionEvent) throws IOException {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }
}
