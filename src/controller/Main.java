package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.ParseException;

public class Main extends Application {

    //Opens login page by using javafx GUI window
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent loginFXML = FXMLLoader.load(getClass().getResource("/resources/view/LoginPage.fxml"));
        primaryStage.setTitle("『Product Catalogue System | Life in MOVE』");
        primaryStage.setScene(new Scene(loginFXML));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) throws Exception {
        FileManagerController.onStartup();
        launch(args);
    }
}
