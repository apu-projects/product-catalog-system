package controller;

import model.object.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileManagerController {

    //loads text files to their path
    protected static File Users = new File("src\\data\\Users.txt");
    protected static File Category = new File("src\\data\\Category.txt");
    protected static File Supplier = new File("src\\data\\Supplier.txt");
    protected static File Catalogue = new File("src\\data\\Catalogue.txt");
    protected static File Product = new File("src\\data\\Products.txt");
    protected static File Log = new File("src\\data\\log\\Log.txt");

    //Getters
    public static final File getUsers() { return Users; }
    public static final File getCategory() { return Category; }
    public static final File getCatalogue() { return Catalogue; }
    public static final File getSupplier() { return Supplier; }
    public static final File getProduct() { return Product; }
    public static final File getLog() { return Log; }

    public static void onStartup() throws Exception {
        BufferedReader reader;
        String str;

        reader = new BufferedReader(new FileReader(getProduct()));
        while ((str = reader.readLine()) != null) {
            String[] productArray = str.split("\\|");
            ProductObject.productList.add(new ProductObject(Integer.parseInt(productArray[0]),
                    productArray[1],productArray[2],Integer.parseInt(productArray[3]),Double.parseDouble(productArray[4]),
                    productArray[5],productArray[6]));
        }

        reader = new BufferedReader(new FileReader(getCategory()));
        while ((str = reader.readLine()) != null) {
            String[] categoryArray = str.split("\\|");
            CategoryObject.categoryList.add(new CategoryObject(categoryArray[0],
                    Integer.parseInt(categoryArray[1]),categoryArray[2]));
        }

        reader = new BufferedReader(new FileReader(getSupplier()));
        while ((str = reader.readLine()) != null) {
            String[] supplierArray = str.split("\\|");
            SupplierUserObject.supplierList.add(new SupplierUserObject(supplierArray[0],
                    Integer.parseInt(supplierArray[1])));
        }

        reader = new BufferedReader(new FileReader(getCatalogue()));
        while ((str = reader.readLine()) != null) {
            String[] catalogueArray = str.split("\\|");
            CatalogueObject.catalogueList.add(new CatalogueObject(catalogueArray[0],Integer.parseInt(catalogueArray[1]),
                    LocalDate.parse(catalogueArray[2]),LocalDate.parse(catalogueArray[3]),catalogueArray[4]));
        }

        reader = new BufferedReader(new FileReader(getUsers()));
        while ((str = reader.readLine()) != null) {
            String[] userArray = str.split("\\|");
            UserObject.userList.add(new UserObject(Integer.parseInt(userArray[0]),userArray[1],
                    userArray[2],userArray[3],userArray[4],
                    userArray[5],userArray[6]));
        }
    }

    //Method writes Logs whenever the user will sign in
    public void LogWriter(String position, String name) throws Exception {
        DateTimeFormatter dateTime = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime localDate = LocalDateTime.now();
        PrintWriter pw = new PrintWriter(new FileWriter(getLog(), true));

        if (position.equals("Administrator")) {
            pw.println("[Admin] (" + name + ") logged into system in (" +dateTime.format(localDate) + ")|");
        } else if (position.equals("Product Manager")) {
            pw.println("[Product Manager] (" + name + ") logged into system in (" +dateTime.format(localDate) + ")|");
        }
        pw.close();
    }

    public static void ProductPageWriter(String action) throws IOException {
        DateTimeFormatter dateTime = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime localDate = LocalDateTime.now();
        PrintWriter pw = new PrintWriter(new FileWriter(getLog(), true));
        for (UserObject userObject: UserObject.userList) {
            if (userObject.getRole().equals("Administrator")) {
                pw.println("[Admin] (" + userObject.getUsername() + ") " + action + " in Product Page in (" +dateTime.format(localDate) + ")|");
                break;
            } else{
                pw.println("[Product Manager] (" + userObject.getUsername() + ") " + action + " in Product Page in (" +dateTime.format(localDate) + ")|");
                break;
            }
        }
        pw.close();
    }
}
