package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.object.ProductObject;
import model.object.UserObject;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class ManageUsersPage implements Initializable {
    @FXML TableView manageUsersTableView;
    @FXML TableColumn manageUsersIDTable;
    @FXML TableColumn manageUsersNameTable;
    @FXML TableColumn manageUsersRoleTable;
    @FXML TableColumn manageUsersAddressTable;
    @FXML TableColumn manageUsersNumberTable;
    @FXML TableColumn manageUsersEmailTable;
    @FXML TableColumn manageUsersPasswordTable;

    @FXML TextField manageUsersIDTextField;
    @FXML TextField manageUsersNameTextField;
    @FXML TextField manageUsersAddressTextField;
    @FXML TextField manageUsersNumberTextField;
    @FXML TextField manageUsersEmailTextField;
    @FXML TextField manageUsersRoleTextField;
    @FXML TextField manageUsersPasswordTextField;

    @FXML ComboBox manageUsersStatusComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        manageUsersIDTable.setCellValueFactory( new PropertyValueFactory<>("userID"));
        manageUsersNameTable.setCellValueFactory( new PropertyValueFactory<>("username"));
        manageUsersPasswordTable.setCellValueFactory( new PropertyValueFactory<>("password"));
        manageUsersRoleTable.setCellValueFactory( new PropertyValueFactory<>("role"));
        manageUsersAddressTable.setCellValueFactory( new PropertyValueFactory<>("address"));
        manageUsersNumberTable.setCellValueFactory( new PropertyValueFactory<>("contactNumber"));
        manageUsersEmailTable.setCellValueFactory( new PropertyValueFactory<>("emailAddress"));
        manageUsersTableView.setItems(UserObject.userList);

        manageUsersStatusComboBox.getItems().add("true");
        manageUsersStatusComboBox.getItems().add("false");
    }

    public void usersSelectClick(MouseEvent mouseEvent) {
        UserObject.userSelectList = manageUsersTableView.getSelectionModel().getSelectedItems();
        manageUsersIDTextField.setText(String.valueOf(UserObject.userSelectList.get(0).getUserID()));
        manageUsersNameTextField.setText(UserObject.userSelectList.get(0).getUsername());
        manageUsersPasswordTextField.setText(UserObject.userSelectList.get(0).getPassword());
        manageUsersAddressTextField.setText(UserObject.userSelectList.get(0).getAddress());
        manageUsersNumberTextField.setText(String.valueOf(UserObject.userSelectList.get(0).getContactNumber()));
        manageUsersEmailTextField.setText(UserObject.userSelectList.get(0).getEmailAddress());
        manageUsersRoleTextField.setText(UserObject.userSelectList.get(0).getRole());
    }

    public void manageUsersUpdateButton(ActionEvent actionEvent) throws IOException {
        UserObject userObject = (UserObject) manageUsersTableView.getSelectionModel().getSelectedItem();
        userObject.setUserID(Integer.parseInt(manageUsersIDTextField.getText()));
        userObject.setUsername(manageUsersNameTextField.getText());
        userObject.setPassword(manageUsersPasswordTextField.getText());
        userObject.setRole(manageUsersRoleTextField.getText());
        userObject.setAddress(manageUsersAddressTextField.getText());
        userObject.setContactNumber(manageUsersNumberTextField.getText());
        userObject.setEmailAddress(manageUsersEmailTextField.getText());

        FileManagerController.ProductPageWriter("Updated user");

        String searchID = String.valueOf(userObject.getUserID());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getUsers())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchID)) {
                        tempArray.add(userObject.getUserID() + "|" +
                                userObject.getUsername() + "|" +
                                userObject.getPassword() + "|" +
                                userObject.getAddress() + "|" +
                                userObject.getContactNumber() + "|" +
                                userObject.getEmailAddress() + "|" +
                                userObject.getRole());
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getUsers())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void manageUsersDeleteButton(ActionEvent actionEvent) throws IOException {
        manageUsersTableView.getItems().removeAll(manageUsersTableView.getSelectionModel().getSelectedItems());
        FileManagerController.ProductPageWriter("Deleted user");

        String searchID = String.valueOf(manageUsersIDTextField.getText());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getUsers())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchID)) {
                        tempArray.toString().trim();
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getUsers())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void manageUsersAddButton(ActionEvent actionEvent) throws IOException {
        UserObject.userList.add(new UserObject(Integer.parseInt(manageUsersIDTextField.getText()),
                manageUsersNameTextField.getText(),manageUsersPasswordTextField.getText(),manageUsersAddressTextField.getText(),manageUsersNumberTextField.getText(),
                manageUsersEmailTextField.getText(),manageUsersRoleTextField.getText()));

        FileManagerController.ProductPageWriter("Added user");

        PrintWriter pw = new PrintWriter(new FileWriter(FileManagerController.getUsers(), true));
        pw.println(UserObject.userList.get(UserObject.userList.size()-1));
        pw.close();
    }

    public void manageUsersBackButton(ActionEvent actionEvent) {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }
}
