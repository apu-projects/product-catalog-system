package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.object.ProductObject;
import model.object.UserObject;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class PersonalInfoPage{
    @FXML private Label personalID;
    @FXML private TextField personalNameTextField;
    @FXML private TextField personalTelNoTextField;
    @FXML private TextField personalAddressTextField;
    @FXML private TextField personalEmailTextField;
    @FXML private TextField personalUsernameTextField;
    @FXML private TextField personalPasswordTextField;

    public void personalShowButton(ActionEvent actionEvent) {
        for (UserObject userObject: UserObject.userList) {
            personalID.setText(String.valueOf(userObject.getUserID()));
            personalNameTextField.setText(userObject.getUsername());
            personalTelNoTextField.setText(userObject.getContactNumber());
            personalAddressTextField.setText(userObject.getAddress());
            personalEmailTextField.setText(userObject.getEmailAddress());
            personalUsernameTextField.setText(userObject.getUsername());
            personalPasswordTextField.setText(userObject.getPassword());
        }
    }

    public void personalBackButton(ActionEvent actionEvent) {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }

    public void personalUpdateButton(ActionEvent actionEvent) throws IOException {
        UserObject userObject = new UserObject();
        userObject.setUserID(Integer.parseInt(personalID.getText()));
        userObject.setUsername(personalUsernameTextField.getText());
        userObject.setPassword(personalPasswordTextField.getText());
        userObject.setAddress(personalAddressTextField.getText());
        userObject.setContactNumber(personalTelNoTextField.getText());
        userObject.setEmailAddress(personalEmailTextField.getText());

        FileManagerController.ProductPageWriter("Updated user settings");

        String searchID = String.valueOf(userObject.getUserID());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getUsers())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchID)) {
                        tempArray.add(userObject.getUserID() + "|" +
                                userObject.getUsername() + "|" +
                                userObject.getPassword() + "|" +
                                userObject.getAddress() + "|" +
                                userObject.getContactNumber() + "|" +
                                userObject.getEmailAddress() + "|" +
                                lineArr[6]);
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getUsers())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }
}
