package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {
    @FXML TextField usernameTextField;
    @FXML PasswordField passwordField;

    VerifyController verify = new VerifyController();

    //loads method to verify login and password
    public void loginButton(ActionEvent actionEvent) throws Exception {
        verify.verifyLogin(usernameTextField.getText(), passwordField.getText());

        if (verify.verifyBoolean) {
            ((Node) actionEvent.getSource()).getScene().getWindow().hide();
            verify.verifyBoolean = false;
        }
    }
}
