package controller;

import com.sun.source.tree.IfTree;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.object.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;


public class AdminPage implements Initializable {
    @FXML TableView productTableView;
    @FXML TableColumn productIDTable;
    @FXML TableColumn productNameTable;
    @FXML TableColumn productCategoryTable;
    @FXML TableColumn productQuantityTable;
    @FXML TableColumn productPriceTable;
    @FXML TableColumn productSupplierTable;
    @FXML TableColumn productCatalogueTable;

    @FXML TextField productIDTextField;
    @FXML TextField productNameTextField;
    @FXML TextField productQuantityTextField;
    @FXML TextField productPriceTextField;

    @FXML ComboBox productCategoryComboBox;
    @FXML ComboBox productCatalogueComboBox;
    @FXML ComboBox productSupplierComboBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        productIDTable.setCellValueFactory( new PropertyValueFactory<>("productID"));
        productNameTable.setCellValueFactory( new PropertyValueFactory<>("productName"));
        productCategoryTable.setCellValueFactory( new PropertyValueFactory<>("productCategory"));
        productQuantityTable.setCellValueFactory( new PropertyValueFactory<>("productQuantity"));
        productPriceTable.setCellValueFactory( new PropertyValueFactory<>("productRetailPrice"));
        productSupplierTable.setCellValueFactory( new PropertyValueFactory<>("supplierName"));
        productCatalogueTable.setCellValueFactory( new PropertyValueFactory<>("productCatalogue"));
        productTableView.setItems(ProductObject.productList);

        productCategoryComboBox.setItems(CategoryObject.categoryList);
        productCatalogueComboBox.setItems(CatalogueObject.catalogueList);
        productSupplierComboBox.setItems(SupplierUserObject.supplierList);
    }

    public void productUpdateButton(ActionEvent actionEvent) throws IOException {
        ProductObject productObject = (ProductObject) productTableView.getSelectionModel().getSelectedItem();
        productObject.setProductID(Integer.parseInt(productIDTextField.getText()));
        productObject.setProductName(productNameTextField.getText());
        productObject.setProductCategory(productCategoryComboBox.getValue().toString().split("\\|")[0]);
        productObject.setProductQuantity(Integer.parseInt(productQuantityTextField.getText()));
        productObject.setProductRetailPrice(Double.parseDouble(productPriceTextField.getText()));
        productObject.setSupplierName(productSupplierComboBox.getValue().toString().split("\\|")[0]);
        productObject.setProductCatalogue(productCatalogueComboBox.getValue().toString().split("\\|")[0]);

        FileManagerController.ProductPageWriter("Updated product");

        String searchID = String.valueOf(productObject.getProductID());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getProduct())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchID)) {
                        tempArray.add(productObject.getProductID() + "|" +
                                productObject.getProductName() + "|" +
                                productObject.getProductCategory() + "|" +
                                productObject.getProductQuantity() + "|" +
                                productObject.getProductRetailPrice() + "|" +
                                productObject.getSupplierName() + "|" +
                                productObject.getProductCatalogue());
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getProduct())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void productSelectClick(MouseEvent mouseEvent) {
        ProductObject.productSelectList = productTableView.getSelectionModel().getSelectedItems();
        productIDTextField.setText(String.valueOf(ProductObject.productSelectList.get(0).getProductID()));
        productNameTextField.setText(ProductObject.productSelectList.get(0).getProductName());
        productCategoryComboBox.setValue(ProductObject.productSelectList.get(0).getProductCategory());
        productQuantityTextField.setText(String.valueOf(ProductObject.productSelectList.get(0).getProductQuantity()));
        productPriceTextField.setText(String.valueOf(ProductObject.productSelectList.get(0).getProductRetailPrice()));
        productSupplierComboBox.setValue(ProductObject.productSelectList.get(0).getSupplierName());
        productCatalogueComboBox.setValue(ProductObject.productSelectList.get(0).getProductCatalogue());
    }

    public void productDeleteButton(ActionEvent actionEvent) throws IOException {
        productTableView.getItems().removeAll(productTableView.getSelectionModel().getSelectedItems());
        FileManagerController.ProductPageWriter("Deleted product");

        String searchID = String.valueOf(productIDTextField.getText());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getProduct())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchID)) {
                        tempArray.toString().trim();
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getProduct())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void productAddButton(ActionEvent actionEvent) throws IOException {
        ProductObject.productList.add(new ProductObject(Integer.parseInt(productIDTextField.getText()),productNameTextField.getText(),
                productCategoryComboBox.getValue().toString().split("\\|")[0],Integer.parseInt(productQuantityTextField.getText()),
                Double.parseDouble(productPriceTextField.getText()),productSupplierComboBox.getValue().toString().split("\\|")[0]
                ,productCatalogueComboBox.getValue().toString().split("\\|")[0]));
        FileManagerController.ProductPageWriter("Added product");

        PrintWriter pw = new PrintWriter(new FileWriter(FileManagerController.getProduct(), true));
        pw.println(ProductObject.productList.get(ProductObject.productList.size()-1));
        pw.close();
    }

    public void productBackButton(ActionEvent actionEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/LoginPage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();

        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }

    public void productManageUsersList(MouseEvent mouseEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/ManageUsersPage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();
    }

    public void productCategoryList(MouseEvent mouseEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/CategoryPage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();
    }

    public void productSupplierList(MouseEvent mouseEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/SupplierListPage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();
    }

    public void productViewLogsList(MouseEvent mouseEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/LogsPage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();
    }

    public void productCatalogueList(MouseEvent mouseEvent) throws IOException {
        Parent Dashboard = FXMLLoader.load(getClass().getResource("/resources/view/CataloguePage.fxml"));
        Stage stage = new Stage();
        stage.setTitle("『Product Catalogue System | Life in MOVE』");
        stage.setScene(new Scene(Dashboard));
        stage.setResizable(false);
        stage.show();
    }
}
