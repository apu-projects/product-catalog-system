package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.object.CategoryObject;
import model.object.ProductObject;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class CategoryPage implements Initializable {
    @FXML TableView categoryTableView;
    @FXML TableColumn categoryIDTable;
    @FXML TableColumn categoryNameTable;
    @FXML TableColumn categoryDescriptionTable;

    @FXML TextField categoryIDTextField;
    @FXML TextField categoryNameTextField;
    @FXML TextField categoryDescriptionTextField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        categoryIDTable.setCellValueFactory( new PropertyValueFactory<>("categoryID"));
        categoryNameTable.setCellValueFactory( new PropertyValueFactory<>("categoryName"));
        categoryDescriptionTable.setCellValueFactory( new PropertyValueFactory<>("categoryDescription"));
        categoryTableView.setItems(CategoryObject.categoryList);
    }

    public void categorySelectClick(MouseEvent mouseEvent) {
        CategoryObject.categorySelectList = categoryTableView.getSelectionModel().getSelectedItems();
        categoryIDTextField.setText(String.valueOf(CategoryObject.categorySelectList.get(0).getCategoryID()));
        categoryNameTextField.setText(CategoryObject.categorySelectList.get(0).getCategoryName());
        categoryDescriptionTextField.setText(CategoryObject.categorySelectList.get(0).getCategoryDescription());
    }

    public void categoryUpdateButton(ActionEvent actionEvent) throws IOException {
        CategoryObject categoryObject = (CategoryObject) categoryTableView.getSelectionModel().getSelectedItem();
        categoryObject.setCategoryID(Integer.parseInt(categoryIDTextField.getText()));
        categoryObject.setCategoryName(categoryNameTextField.getText());
        categoryObject.setCategoryDescription(categoryDescriptionTextField.getText());

        FileManagerController.ProductPageWriter("Updated category");

        String searchName = categoryObject.getCategoryName();
        System.out.println(categoryObject.getCategoryName());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getCategory())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.add(categoryObject.getCategoryName() + "|" +
                                categoryObject.getCategoryID() + "|" +
                                categoryObject.getCategoryDescription());
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getCategory())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void categoryDeleteButton(ActionEvent actionEvent) throws IOException {
        categoryTableView.getItems().removeAll(categoryTableView.getSelectionModel().getSelectedItems());
        FileManagerController.ProductPageWriter("Deleted product");

        String searchName = String.valueOf(categoryNameTextField.getText());
        ArrayList<String> tempArray = new ArrayList<>();

        try {
            try (FileReader fr = new FileReader(FileManagerController.getCategory())) {
                Scanner reader = new Scanner(fr);
                String line;
                String[] lineArr;

                while ((line = reader.nextLine()) != null) {
                    lineArr = line.split("\\|");
                    if (lineArr[0].equals(searchName)) {
                        tempArray.toString().trim();
                    } else {
                        tempArray.add(line);
                    }
                }
                fr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

        try {
            try (PrintWriter pr = new PrintWriter(FileManagerController.getCategory())) {
                for (String str: tempArray) {
                    pr.println(str);
                }
                pr.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void categoryAddButton(ActionEvent actionEvent) throws IOException {
        CategoryObject.categoryList.add(new CategoryObject(categoryNameTextField.getText(),
                Integer.parseInt(categoryIDTextField.getText()),categoryDescriptionTextField.getText()));

        FileManagerController.ProductPageWriter("Added category");

        PrintWriter pw = new PrintWriter(new FileWriter(FileManagerController.getCategory(), true));
        pw.println(CategoryObject.categoryList.get(CategoryObject.categoryList.size()-1));
        pw.close();
    }

    public void categoryBackButton(ActionEvent actionEvent) throws IOException {
        ((Node) actionEvent.getSource()).getScene().getWindow().hide();
    }
}
