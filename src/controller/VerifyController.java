package controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import model.object.Administrator;
import model.object.ProductManager;
import model.object.UserObject;

import java.io.BufferedReader;
import java.io.FileReader;

public class VerifyController {
    protected static String dashboard;
    public static boolean verifyBoolean;

    FileManagerController fileManager = new FileManagerController();

    //This is the method to verify the user's username and password
    public void verifyLogin(String username, String password) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileManager.getUsers()));
            String str;

            //loads all information about user to User object
            while ((str = reader.readLine()) !=null) {
                String[] userArray = str.split("\\|");
                if (userArray[1].equals(username) && userArray[2].equals(password)) {
                    if (userArray[6].equals("Administrator")) {
                        UserObject.userList.add(new Administrator(Integer.parseInt(userArray[0]),userArray[1],
                                userArray[2],userArray[3],userArray[4],userArray[5],
                                userArray[6]));
                        fileManager.LogWriter(userArray[6], userArray[1]);
                        dashboard = "/resources/view/AdminPage.fxml";
                    } else if (userArray[6].equals("Product Manager")) {
                        UserObject.userList.add(new ProductManager(Integer.parseInt(userArray[0]),userArray[1],
                                userArray[2],userArray[3],userArray[4],userArray[5],
                                userArray[6]));
                        fileManager.LogWriter(userArray[6], userArray[1]);
                        dashboard = "/resources/view/ManagerPage.fxml";
                    }

                    //Opens admin or manager dashboard
                    Parent Dashboard = FXMLLoader.load(getClass().getResource(dashboard));
                    Stage stage = new Stage();
                    stage.setTitle("『Product Catalogue System | Life in MOVE』");
                    stage.setScene(new Scene(Dashboard));
                    stage.setResizable(false);
                    stage.show();

                    verifyBoolean = true;
                }
            }

            if (!verifyBoolean) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("There are some problems");
                alert.setContentText("Wrong username or password");
                alert.showAndWait();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
